# Создайте массив состоящий из 5ти словарей
# --- > world = [
#     {'apple' : '12p.'},
#     {'carrot' : '15p.'},
#     {'cabbage' : '20p.'},
#     {'pear' : '10p.'},
#     {'potato' : '17p.'}
# ]


# Создайте массив состоящий из 5ти массивов с любыми значениями
# --- > world = [1, ['god', [2, ["fool", [3]]]]]


# Замените в ранее созданном массиве несколько значений
# --- > world = ['sur', [5, ['vol', ["fool", [3]]]]]


# Создайте цикл для первого масссива
world = [
    {'apple' : '12p.'},
    {'carrot' : '15p.'},
    {'cabbage' : '20p.'},
    {'pear' : '10p.'},
    {'potato' : '17p.'}
]

for arr in world:
    print(arr, end=', ')


# Создайте цикл для второго масссива
world = [1, ['god', [2, ["fool", [3]]]]]

for arr in world:
    print(arr, end=', ')


# Создайте массив состоящий из 10 цифр от 0 и до 20
# --- > world = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19]


# Пройдитесь циклоом по вышесозданному муссиву и выведите все чита которые больше 10ти
world = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19]

for arr in world:
    if arr > 10:
        print(arr, end=' ')



# Создайте словарь который содержит в себе все типы данных которые мы проходили (ключи на ваше усморение)
# --- > arr ={
#     "name" : "Adil",
#     "age" : 17,
#     }


# Создайте словарь который содержит в себе 5 разных массивов
arr ={
    "name":["Adil","Malik","Nurik"],
    "age":[17,13,15],
    "gender":["man","man","man"],
    "class":[10,8,9],
    "idi_zakonchilis":[1,2,3]
    }

print(arr["age"])


# Создайте условия if / else
arr = int(input())
if arr > 0:
    print(arr)
else:
    print("-")


# Создайте условия if / elif / else
arr = int(input())
if arr > 0:
    print(arr)
elif arr < 20:
    print(0)
else:
    print("-")


# Создайте условия if / elif / elif / else
arr = int(input())
if arr > 0:
    print(arr)
elif arr < 20:
    print(0)
elif arr == 10:
    print(10)
else:
    print("-")


# Создайте условия цикл который проходится по массиву в который содержит в себе условия if / elif / else
arr = [1, 2, 3]

for i in arr:
    if i == 2:
        print(i)
    elif i < 20:
        print(0)
    else:
        print("-")
